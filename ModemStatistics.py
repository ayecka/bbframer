#!/usr/bin/python3
# Listen on SDD messages in TLV format and parse them
# To enable SDD on SR1Pro set FPGA reg 0x20 to interval in msec
# SDD uses multi cast - make sure proper routing in receiving PC .e.g route add 225.1.1.1 gw 192.168.10.67
# Applicable for SR1Pro - FPGA 9.10b30 and up
# baruchk@ayecka.com

# V1.0 12-10-18 - Base Line
# V1.1 12-10-18 - Support test pattern TLV F0
# V1.1 21-1-19 - Added Avg Power


import threading
import os
import sys
from Sdd import Sdd
from UdpMulticastReceiver import UdpMulticastReceiver
from flask import Flask, send_from_directory, jsonify, Response
from Tlv import Tlv
from datetime import datetime
E8 = True  # we use this flag to avoid display of second Demod Status in Sr1Pro . Will be used in SM1x


python_root = sys.path[0]
app = Flask("__main__")
sdd = None


@app.route("/", methods=["GET"])
def get_page():
    return send_from_directory(os.path.join(python_root, "html"), "index.html")


@app.route("/javascript/<path:path>", methods=["GET"])
def send_scripts(path):
    return send_from_directory(os.path.join(python_root, "html", "javascript"), path)


@app.route("/stylesheets/<path:path>", methods=["GET"])
def send_style(path):
    return send_from_directory(os.path.join(python_root, "html", "stylesheets"), path)


@app.route("/status", methods=["GET", "POST"])
def get_status():
    sdd = Sdd()
    if sdd is not None:
        jdata = [
            {"name": "FPGA version", "data": sdd.FpgaVersion},
            {"name": "EsNo", "data": sdd.Esno},
            {"name": "RF Relocks", "data": sdd.rfRelocks},
            {"name": "Biterate", "data": sdd.testPatternBitRate},
            {"name": "Good Frames", "data": sdd.goodFrames},
            {"name": "Missed Frames", "data": sdd.missedFrames},
            {"name": "Bad Frames", "data": sdd.badFrames}
        ]
        response = jsonify(jdata)
    else:
        response = Response(
            "<h1>No data - connection to the modem is not active</h1>")

    response.status_code = 200
    return response


'''
@app.route("/state", methods=["GET", "POST"])
def get_state():
    jdata = {
                "state": "runing",
                "pls": 1,
                "gap": 0.01,
                "host": "192.168.2.102",
                "dport": 1234,
                "sport": 5432
            }
    response = jsonify(jdata)
    response.status_code = 200
    return response

@app.route("/start", methods=["GET", "POST"])
def get_state():
    jdata = {
                "state": "runing",
                "pls": 1,
                "gap": 0.01,
                "host": "192.168.2.102",
                "dport": 1234,
                "sport": 5432
            }
    response = jsonify(jdata)
    response.status_code = 200
    return response
'''


def main():
    global sdd
    # start listening on multicast socket
    print("initializing socket to listen to 225.1.1.1:7421")
    sock = UdpMulticastReceiver(group=MCAST_GRP, port=MCAST_PORT)

    msgCounter = 0

    try:
        # target = app.run(host="0.0.0.0", port=8080, debug=True, use_reloader=False)
        threading.Thread(target=lambda: app.run(
            host="0.0.0.0", port=8080, debug=True, use_reloader=False)).start()
        pass
    except Exception as ex:
        print("Failed to start http on port 8080")

    sdd = Sdd()
    while True:  # here we start listening on the socket
        x = sock.Recv(10240)
        if x is None:
            continue
        msgCounter += 1
        print(chr(27) + "[2J")  # cls
        print("----   "+str(datetime.now()) +
              "  msgCounter = "+str(msgCounter)+"   ----")
        tlv = Tlv()
        tlv.load(x)

        while tlv.get_next():  # tlv.t != 0:
            ret_str = sdd.Parser(tlv.t, tlv.l, tlv.v)
            if not sdd.cafe:
                print("first TLV is not CAFE")
                exit(11)
            # if ret_str is not None:
            #    print (ret_str)
        print(f'FPGA Version   : {sdd.FpgaVersion}')
        print(f'EsNo           : {sdd.Esno}')
        print(f'RF Relocks     : {sdd.rfRelocks}')
        print(f'---------------------------------------------------')
        print(f'\tBiterate     : {sdd.testPatternBitRate:,}')
        print(f'\tGood frames  : {sdd.goodFrames:,}')
        print(f'\tMissed frames: {sdd.missedFrames:,}')
        print(f'\tBad frames   : {sdd.badFrames:,}')


if __name__ == "__main__":
    main()
