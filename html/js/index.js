const settings_key = 'bb-tester-key';

const settings = [
    //{ id: 'selectid', type: 'select', text: 'Select', options: [1, 2, 3, 4, 5] },
    { id: 'pls', type: 'select', text: 'PLS', sub: 'integer', default: 7, options: [5, 7, 31, 41, 93] },
    { id: 'gap', type: 'input', text: 'GAP', sub: 'float', default: 0.001 },
    { id: 'packets', type: 'input', text: 'Packets', sub: 'integer', default: 1000 },
    { id: 'host', type: 'ip', text: 'Host', default: '192.168.2.102' },
    { id: 'dport', type: 'input', text: 'Destination Port', sub: 'integer', default: 1234 },
    { id: 'sport', type: 'input', text: 'Source Port', sub: 'integer', default: 5432 },
]

const get_status = () => {
    fetch(new Request('http://' + window.location.host + '/status', { method: 'GET' }))
        .then(resp => resp.json())
        .then(data => updateTable(data))
        .catch(error => console.error(error)).finally(() => setTimeout(get_status, 1000));
}

const updateTable = (data) => {
    const table = $('#status tbody');

    data.forEach(x => {
        let tr = table.find('tr[name="' + x.name + '"]');
        if (tr.length == 0) {
            tr = $('<tr name="' + x.name + '"/>').appendTo(table);
            tr.append($('<td>' + x.name + '</td>'));
            tr.append($('<td>' + x.data + '</td>'));
        }
        else {
            tr.find('td:eq(1)').text(x.data);
        }
    })
}

const updateState = (data) => {
    console.log(data);

    const button = $('#start');
    const state = data.state || '';

    if (state.includes('run')) {
        button.text('Stop');

        settings.forEach(s => {
            const element = $('#' + s.id);
            const value = data[s.id];
            switch (s.type) {
                case 'select':
                    element.prop('disabled', true);
                    element.find('[value="' + value + '"]').prop('selected', true);
                    break;

                case 'ip':
                    element.find('input').prop('readonly', true)
                    element.find('i').prop('disabled', true);
                    const handler = element.data('handler');
                    handler.setIp(value);
                    break;

                default:
                    element.val(value);
                    element.prop('readonly', true);
                    break;
            }
        });

        window.localStorage.setItem(settings_key, JSON.stringify(get_configuration()));
    }
    else {
        settings.forEach(s => {
            const element = $('#' + s.id);
            switch (s.type) {
                case 'select':
                    element.prop('disabled', false);
                    break;

                case 'ip':
                    element.find('input').prop('readonly', false)
                    element.find('i').prop('disabled', false);
                    break;

                default:
                    element.prop('readonly', false);
                    break;
            }
        });

        button.text('Start');
    }
};

const get_state = () => {
    fetch(new Request('http://' + window.location.host + '/state', { method: 'GET' }))
        .then(resp => resp.json())
        .then(data => updateState(data))
        .catch(error => console.error(error)).finally(() => setTimeout(get_state, 1000));
}

$(() => {
    const body = $('#settings tbody');
    settings.forEach(x => {
        const tr = $('<tr/>').appendTo(body);
        tr.append($('<td class="align-middle">' + x.text + '</td>'));
        switch (x.type) {
            case 'select':
                const select = $('<select id="' + x.id + '" class="form-control"/>').appendTo(tr);
                (x.options || []).forEach(y => {
                    select.append($('<option value="' + y + '">' + y + '</option>'));
                });
                break;
            case 'ip':
                const ip = $('<td id="' + x.id + '" class="text-center"/>').appendTo(tr);
                const handler = ip.ipInput();
                handler.setIp('127.0.0.1');
                ip.data('handler', handler);
                break;
            default:
                tr.append($('<td><input id="' + x.id + '" class="form-control input-sm text-center" value="' + x.default + '"></td>'));
                break;
        }
    });

    try {
        const configuration = JSON.parse(window.localStorage.getItem(settings_key));
        if (configuration) {
            configuration.state = 'running';
            updateState(configuration);
        }
    }
    catch (err) {

    }

    get_status();
    get_state();
})

const get_configuration = () => {
    const data = {};
    settings.forEach(x => {
        if (x.type === 'ip') {
            const ip = $('#' + x.id);
            const handler = ip.data('handler');
            data[x.id] = handler.getIp();
            return;
        }
        switch (x.sub) {
            case 'string':
                data[x.id] = $('#' + x.id).val() || '';
                break;
            case 'integer':
                data[x.id] = +$('#' + x.id).val() | 0;
                break;
            default:
                data[x.id] = +$('#' + x.id).val();
                break;
        }
    })
    return data;
}

const start = () => {
    const button = $('#start');
    if (button.text() === 'Start') {
        fetch(new Request('http://' + window.location.host + '/start', { method: 'POST', body: JSON.stringify(get_configuration()) }))
            .then(resp => resp.json())
            .then(data => console.log('start response', data))
            .catch(error => console.error(error));
    }
    else {
        fetch(new Request('http://' + window.location.host + '/stop', { method: 'GET' }))
            .then(resp => resp.json())
            .then(data => console.log('stop response', data))
            .catch(error => console.error(error));
    }
}
