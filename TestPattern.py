from Pdu import Pdu
import struct


class TestPatern(Pdu):
    @staticmethod
    def BuildTestDataBuffer():
        MAX_TEST_BUFFER_SIZE = 10000
        Buffer = bytearray()
        for i in range(0, MAX_TEST_BUFFER_SIZE):
            x = i % 256
            Buffer.append(x)
        return Buffer

    TestCounter = 0
    TestDataBuffer: bytearray = BuildTestDataBuffer.__func__()

    def __init__(self, payloadSize: int, BBHLen: int = 10) -> None:
        super().__init__()
        self.payloadSize = payloadSize
        self.BBHLen = BBHLen

    def Build(self) -> None:
        self.maketestPatternData()
        return self.pdu

    def maketestPatternData(self) -> bytearray:
        # Counter
        self.pdu += struct.pack('>i', (TestPatern.TestCounter % 0xFFFFFFFF))

        # Length
        # excludign the BBFHeader includign the test pattern header
        self.pdu += struct.pack('>h', (self.payloadSize))

        # Data
        self.pdu += self.TestDataBuffer[:(self.payloadSize - self.BBHLen - 6)]

        TestPatern.TestCounter += 1
        return self.pdu

    @property
    def PatternSize(self) -> int:
        return self.size - self.offset

    @property
    def HeaderSize(self) -> int:
        return 6


def main():
    tp = TestPatern(2001)
    tp.Build()
    print(tp.PatternSize)
    print(tp.size)
    print(tp.pdu)


if __name__ == '__main__':
    main()
