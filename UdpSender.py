import socket
import threading
from queue import Queue
from time import sleep
from dataclasses import dataclass
from Pdu import Pdu


@dataclass
class DataPacket:
    def __init__(self, host: str, port: int, data: bytearray):
        self.host = host
        self.port = port
        self.data = data

    host: str
    port: int
    data: bytearray


class UdpSender:

    sock = None

    def __init__(self, localIP: str, LocalPort: int, queueSize: int = 5) -> None:
        self.stopRun = False
        self.queueSize = queueSize
        self.SendQueue = Queue(queueSize)
        if UdpSender.sock is not None:
            UdpSender.sock.close()
        if self.initSock(localIP, LocalPort) is False:
            raise Exception("Error open socket")
        UdpSender.socket_in_use = True

    def initSock(self, localIP: str, LocalPort: int) -> bool:  # open socket for UDP communication
        try:
            UdpSender.sock = socket.socket(
                socket.AF_INET, socket.SOCK_DGRAM)  # Socket
        except OSError as msg:
            UdpSender.sock = None
            print("Failed to create socket. Error Code : " +
                  str(msg[0]) + ' Message ' + msg[1])
            return False

        try:
            UdpSender.sock.setblocking(True)
            UdpSender.sock.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 5)      
        except OSError as msg:
            # print ('Failed to set blocking to non socket. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
            UdpSender.sock = None
            print("failed to set blocking"+msg)
            return False
        try:
            UdpSender.sock.bind((localIP, LocalPort))
        except OSError as msg:
            print("Failed to bind socket. Error Code : " +
                  str(msg[0]) + ' Message ' + msg[1])
            return False
            # return ("socket opened and binded to "+ str(localIP)+":"+str(port))
        return True

    def Start(self) -> None:
        self.workerThread = threading.Thread(target=self.worker, daemon=True)
        self.workerThread.start()

    def Stop(self) -> None:
        self.stopRun = True
        sleep(0.01)
        self.workerThread.join()

    def Size(self) -> int:
        return self.SendQueue.qsize()

    def Add(self, deviceIP: str, port: int, msg: bytearray) -> None:
        dp = DataPacket(deviceIP, port, msg)
        self.AddDataPacket(dp)

    def AddPdu(self, deviceIP: str, port: int, pdu: Pdu) -> None:
        dp = DataPacket(deviceIP, port, pdu.pdu)
        self.AddDataPacket(dp)

    def Empty(self):
        return self.SendQueue.empty()

    def AddDataPacket(self, dp: DataPacket) -> None:
        self.SendQueue.put(dp, block=True)

    def worker(self) -> None:
        self.stopRun = False
        while not self.stopRun:
            try:
                if not self.SendQueue.empty():
                    dp: DataPacket = self.SendQueue.get(block=True, timeout=0.5)
                    y = UdpSender.sock.sendto(dp.data, (dp.host, dp.port))
                else:
                    sleep(0.001)
            except Exception as ex:  # timeout
                self.stopRun = True
        #sleep(10)
        #UdpSender.sock.close()


def main():
    send_udp = UdpSender('192.168.1.85', 5432, 100)
    send_udp.Start()
    data = bytearray(2001)

    # data.append(2001)
    for i in range(1, 1000000):
        send_udp.Add('192.168.2.125', 5432, data)


if __name__ == '__main__':
    main()
