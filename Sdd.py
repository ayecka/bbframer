from collections import deque
from singleton_decorator import singleton


@singleton
class Sdd:

    def __init__(self) -> None:
        self.powerIndicators = deque([0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        self.cafe: bool = False
        self.FpgaVersion: str = ""

        self.modem_id: int = 0
        self.modem_serial = 0

        self.demodLockDefenitive: int = 0
        self.demodInAcqu: int = 0
        self.demodInSearch: int = 0
        self.avgPowerIndicator: int = 0
        self.momentaryPowerIndicator: int = 0

        self.testPatternBitRate = 0
        self.goodFrames = 0
        self.badFrames = 0
        self.missedFrames = 0
        self.rfRelocks = 0
        self.Esno = 0

    def Parser(self, t, L, V) -> str:
        ret_str = None
        if t == 255:  # CAFE
            self.cafe = True
            ret_str = ("CAFE")
            if int(V[0]) != 0xca or int(V[1]) != 0xfe:
                ret_str = ("Bad Cafe")
                self.cafe = False
            return ret_str

        if t == 1:  # FPGA version
            self.FpgaVersion = f'{V[0]}.{V[1]}b{V[2]}{V[3]}'
            return (f'FPGA Version = {V[0]}.{V[1]}b{V[2]}{V[3]}')

        if t == 232:  # Link status
            ret_str = "Link status  "
            self.modem_id = V[0]
            ret_str += (f"Modem ID: {V[0]}\n")

            self.modem_serial = V[1]
            ret_str += (f"Demod#: {V[1]}\n")

            ret_str += ("Raw message : ")
            for i in V:
                ret_str += (f'{i} ,')
            ret_str += "\n"
            # Bit(02)	Demod Lock Definitive - demodulator definitively locked
            self.demodLockDefenitive = (V[3] & 4)/4
            # Bit(01)	Demod Tracked - 1: demodulator in acquistion mode (DMDFLYW/flywheel_cpt>= 3), 0: demodulator in search mode
            self.demodInAcqu = (V[3] & 2)/2
            # Bit(00)	Demod Locked - intermediate lock (before tuner re-centering)
            self.demodInSearch = V[3] & 1
            ret_str += (
                f'Definitive lock = {+self.demodLockDefenitive} InAcqu={self.demodInAcqu} InSearch={(self.demodInSearch)}\n')
            if self.demodLockDefenitive:
                x = (V[10] << 8)+V[11]
                ret_str += (f'EsNo [0.1db]={x}\n')
                self.Esno = x

                Sdd().pwoerIndicator = V[8]*256+V[9]
                # pushing new values, poping old values and calculating avg over 10 samples
                self.powerIndicators.appendleft(Sdd().pwoerIndicator)
                self.powerIndicators.pop()
                x = 0
                for i in self.powerIndicators:
                    x = x+i
                self.avgPowerIndicator = x/len(self.powerIndicators)
                self.momentaryPowerIndicator = Sdd().pwoerIndicator
                ret_str += (f'avgPowerIndicator = {self.avgPowerIndicator} \n')
                # Power indicator has meaning even if not locked
                ret_str += (
                    f'momentaryPowerIndicator = {Sdd().pwoerIndicator}\n')
            return ret_str

        if t == 240:  # BBF/TS Test Stream Pattern State
            ret_str = ("Test Pattern statistics")
            self.modem_id = V[0]
            ret_str += f"Modem ID={V[0]}, "

            self.modem_serial = V[1]
            ret_str += f"Demod#={V[1]}\n"
            self.demod_num = V[1]
            ret_str += ("Raw message :")
            for i in V:
                ret_str += f'{i}, '
            ret_str += '\n'

            self.testPatternBitRate = (
                V[4] << 24)+(V[5] << 16)+(V[6] << 8)+V[7]
            ret_str += f"Bit rate = {format(self.testPatternBitRate,',d')}\n"
            self.goodFrames = (V[8] << 24)+(V[9] << 16)+(V[10] << 8)+V[11]
            ret_str += f"Good Frames =  {format(self.goodFrames,',d')}\n"
            self.badFrames = (V[12] << 24)+(V[13] << 16)+(V[14] << 8)+V[15]
            ret_str += f"Bad Frames = {format(self.badFrames,',d')}\n"
            self.missedFrames = (V[16] << 24)+(V[17] << 16)+(V[18] << 8)+V[19]
            ret_str += f"Missed Frames = {format(self.missedFrames,',d')}\n"
            self.rfRelocks = (V[20] << 24)+(V[21] << 16)+(V[22] << 8)+V[23]
            ret_str += f"RF re-locks = {format(self.rfRelocks,',d')}\n"
            return ret_str
