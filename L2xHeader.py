from Pdu import Pdu


class L2xHeader(Pdu):
    L2x_Frame_Count = 0

    def __init__(self, pls: int, slice: int = 1) -> None:
        super().__init__()
        self.pls: int = pls
        self.slice: int = slice

    def Build(self) -> bytearray:
        # L.3 header = L.2
        self.pdu.append(0xC9)
        self.pdu.append(self.pls)
        self.pdu.append(self.slice)
        self.pdu.append(0x00)  # Reserved

        self.pdu.append(L2xHeader.L2x_Frame_Count % 0xff)
        L2xHeader.L2x_Frame_Count += 1

        return self.pdu

    def pls(self) -> bytearray:
        return self.pls


def main():
    p = L2xHeader(5)
    p.makeL2xHeader()
    print(p.size)
    print(p.pdu)


if __name__ == '__main__':
    main()
