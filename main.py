#!/usr/bin/python3
from Sdd import Sdd
import sys
import os
from flask import Flask, send_from_directory, request, jsonify
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from logging import basicConfig, getLogger, INFO
from StatisticsListener import StatisticsListener
from Sr1Tester import Sr1Tester
from dataclasses import dataclass, asdict
import json
from dataclasses_json import dataclass_json

Version = "1.1"

@dataclass_json
@dataclass
class State:
    state: str = "stopped"
    pls: int = 7
    gap: float = 0.001
    host: str = '192.168.2.102'
    dport: int = 1234
    sport: int = 5432
    packets: int = 1000


state = State()
tester = Sr1Tester(state.pls)

python_root = sys.path[0]
app = Flask("__main__")

VERSION = '1.0'


@app.route("/", methods=["GET"])
def get_page():
    return send_from_directory(os.path.join(python_root, "html"), "index.html")


@app.route("/favicon.ico", methods=["GET"])
def get_icon():
    return send_from_directory(os.path.join(python_root, "html"), "favicon.ico")


@app.route("/js/<path:path>", methods=["GET"])
def send_scripts(path):
    return send_from_directory(os.path.join(python_root, "html", "js"), path)


@app.route("/css/<path:path>", methods=["GET"])
def send_style(path):
    return send_from_directory(os.path.join(python_root, "html", "css"), path)


@app.route("/status", methods=["GET", "POST"])
def get_status():
    global tester
    sdd = Sdd()
    if sdd is not None:
        jdata = [
            {"name": "FPGA version", "data": sdd.FpgaVersion},
            {"name": "EsNo", "data": sdd.Esno},
            {"name": "RF Relocks", "data": sdd.rfRelocks},
            {"name": "Biterate", "data": sdd.testPatternBitRate},
            {"name": "Good Frames", "data": sdd.goodFrames},
            {"name": "Missed Frames", "data": sdd.missedFrames},
            {"name": "Bad Frames", "data": sdd.badFrames},
            {"name": "Sent packets", "data" : tester.get_packet_sent()},
            {"name": "Sent time ~", "data" : tester.get_total_time()},
            {"name": "Sent rate[bps] ~", "data" : tester.get_bps()}
        ]
    else:
        return {"result": "error", "description": "No connection to the modem"}, 599
    return jsonify(jdata), 200


@app.route("/state", methods=["GET", "POST"])
def get_state():
    global state
    update_state()
    return asdict(state), 200


@app.route("/start", methods=["POST"])
def start():
    global tester, state

    update_state()
    if tester.is_alive():
        return {"result": "error", "description": "Test alreay running"}, 503

    content = request.data.decode('utf-8')

    state = State(**(json.loads(content)))
    state.state = "running"

    tester = Sr1Tester(pls=state.pls, host=state.host,
                       gap=state.gap, dport=state.dport, sport=state.dport)
    tester.Start(state.packets)
    return {"result": state.state}, 200


@app.route("/stop", methods=["GET", "POST"])
def stop():
    global state
    tester.Stop()
    state.state = "stopped"
    return {"result": state.state}, 200


def update_state():
    global tester, state
    if tester.is_alive():
        state.state = 'running'
    else:
        state.state = 'stopped'


def main() -> None:
    parser = ArgumentParser(
        description=f'Base Frame Test (version {VERSION})', formatter_class=ArgumentDefaultsHelpFormatter)

    # parser.add_argument('-p', '--pls',  nargs='?', const=1, default=7, type=int, choices=[5,7,31,41,93], help='PLS. can be oner of (5,7,31,41,93)')
    # parser.add_argument('-g', '--gap',  nargs='?', const=1,  type=float,  default=0.0,help='Interpacket gap time in seconds')
    # parser.add_argument('-i', '--host', nargs='?', const=1, default='192.168.1.102', type=str, help='Destination ip address xxx.xxx.xxx.xxx')
    # parser.add_argument('-n', '--packets', nargs='?', default=1000, type=int, help='Number of frames to send [0x1000]')
    # parser.add_argument('-d', '--dport', nargs='?', const=1, default=1234, type=int, help='Destination udp port')
    # parser.add_argument('-s', '--sport', nargs='?', const=1,  default=5432, type=int, help='Source udp port')
    parser.add_argument('-l', '--loglevel', nargs='?', const=1,  default=INFO,
                        type=int, choices=[0, 10, 20, 30, 40, 50], help='Set log level')
    parser.add_argument('-p', '--port', type=int, default=8080, help='Set web port [8000]')
    args = parser.parse_args()

    '''
    state.pls = args.pls
    state.host = args.host
    state.dport = args.dport
    state.sport - args.sport
    state.gap = args.gap
    state.packets = args.packets
    '''
    basicConfig(filename='base_tester.log',
                encoding='utf-8', level=args.loglevel)
    log = getLogger(__name__)

    statisticsListener = StatisticsListener(print_to_console=False)
    statisticsListener.start()
    msg = f'*********  Web address is localhost:{args.port}  *********'
    print ('*' * len(msg))
    print (msg)
    print ('*' * len(msg))
    app.run(host="0.0.0.0", port=args.port, debug=True, use_reloader=False)

    '''
    print()      
    print(f'Configuration:')
    print(f'\t Sending {args.packets} from 0.0.0.0:{args.sport} ==> {args.host}:{args.dport}')
    print(f'\t Using time gap of {args.gap} [sec] with PLS={args.pls} (payload={BaseFrame.PayloadSize(args.pls)})')
    print()
    
    input("Press Enter to continue...")
    
    sl = StatisticsListener(print_to_console=False)
    sl.start()
    sr1 = Sr1Tester(pls=args.pls, host=args.host,gap=args.gap,dport=args.dport,sport=args.dport)
    sr1.Start(args.packets)
    
    while(sr1.is_alive()):
        sleep(1)
        print(chr(27) + "[2J")
        print (f"Reveive {sl.get_frame_count()} statistics packets")
    
    while not sr1.finish_to_send():
        sleep(0.1)
        
    sl.Stop()
    print ()
    print ("Send statistics")
    print ("---------------")
    print (f'{sr1.get_packet_sent()} packet sent')
    print (sr1.get_result())
    
    print()
    print(sl.get_result())
    '''


if __name__ == '__main__':
    main()
