class Pdu:
    def __init__(self, offset: int = 0) -> None:
        self.offset = offset
        if offset > 0:
            self.pdu = bytearray(len)
        else:
            self.pdu = bytearray()

    @property
    def size(self) -> int:
        return len(self.pdu)

    @property
    def data(self) -> bytearray:
        return self.pdu

    def Add(self, data: bytearray) -> bytearray:
        self.pdu += data
        return self.pdu

    def __len__(self) -> int:
        return len(self.pdu)

    def __iadd__(self, data) -> object:
        self.pdu += data
        return self


def main():
    pdu = Pdu()
    pdu.append(bytearray([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]))
    print(pdu.size)
    print(pdu.data)


if __name__ == '__main__':
    main()
