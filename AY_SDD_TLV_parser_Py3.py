#!/usr/bin/python
# Listen on SDD messages in TLV format and parse them
# To enable SDD on SR1Pro set FPGA reg 0x20 to interval in msec
# SDD uses multi cast - make sure proper routing in receiving PC .e.g route add 225.1.1.1 gw 192.168.10.67
# Applicable for SR1Pro - FPGA 9.10b30 and up
# baruchk@ayecka.com

# V1.0 12-10-18 - Base Line
# V1.1 12-10-18 - Support test pattern TLV F0
# V1.1 21-1-19 - Added Avg Power

import socket
import struct
import ctypes
from datetime import datetime
from collections import deque

MCAST_GRP = '225.1.1.1'  # hard coded in SR1Pro
MCAST_PORT = 7421  # 7421
IS_ALL_GROUPS = True
E8 = True  # we use this flag to avoid display of second Demod Status in Sr1Pro . Will be used in SM1x
powerIndicators = deque([0, 0, 0, 0, 0, 0, 0, 0, 0, 0])


def SDDParser(T, L, V):
    print("T = "+str(T),)
    print("L = "+str(L),)
    print("   ",)

    if t == 255:  # CAFE
        print('\033[1m' + " CAFE"+'\033[0m')
        if int(V[0]) != 0xca or int(V[1]) != 0xfe:
            print("Bad Cafe")
            exit(12)

    if t == 1:  # FPGA version
        print('\033[1m' + "FPGA Version = "+'\033[0m' +
              str(V[0])+"."+str(V[1])+"b"+str(V[2])+str(V[3]))

    if t == 232:  # Link status
        print('\033[1m' + "Link status  "+'\033[0m',)
        print("Modem ID: ", V[0],)
        print("Demod#: ", V[1],)
        print("Raw message :", end=" "),
        for i in V:
            print(str(i), end=",")
        print()
        # Bit(02)	Demod Lock Definitive - demodulator definitively locked
        demodLockDefenitive = (V[3] & 4)/4
        # Bit(01)	Demod Tracked - 1: demodulator in acquistion mode (DMDFLYW/flywheel_cpt>= 3), 0: demodulator in search mode
        demodInAcqu = (V[3] & 2)/2
        # Bit(00)	Demod Locked - intermediate lock (before tuner re-centering)
        demodInSearch = V[3] & 1
        print("Definitive lock= "+str(demodLockDefenitive)+" InAcqu= " +
              str(demodInAcqu)+" InSearch= "+str(demodInSearch))
        if demodLockDefenitive:
            x = (V[10] << 8)+V[11]
            y = ctypes.c_short(x)
            print("EsNo [0.1db]= ", str(y)[8:-1])
            pwoerIndicator = V[8]*256+V[9]
            # pushing new values, poping old values and calculating avg over 10 samples
            powerIndicators.appendleft(pwoerIndicator)
            powerIndicators.pop()
            x = 0
            for i in powerIndicators:
                x = x+i
            avgPowerIndicator = x/len(powerIndicators)
            print("avgPowerIndicator = "+str(avgPowerIndicator))
            # Power indicator has meaning even if not locked
            print("momentaryPowerIndicator = ", str(pwoerIndicator))

    if t == 240:  # BBF/TS Test Stream Pattern State
        print('\033[1m' + "Test Pattern statistics  "+'\033[0m'),
        print("Modem ID=", V[0], end=",")
        print(" Demod#=", V[1], end=",")
        print("Raw message ", end=","),
        for i in V:
            print(str(i), end=",")
        print()

        testPatternBitRate = (V[4] << 24)+(V[5] << 16)+(V[6] << 8)+V[7]
        print("Bit rate =  " + format(testPatternBitRate, ',d'))
        goodFrames = (V[8] << 24)+(V[9] << 16)+(V[10] << 8)+V[11]
        print("Good Frames =  " + format(goodFrames, ',d'))
        badFrames = (V[12] << 24)+(V[13] << 16)+(V[14] << 8)+V[15]
        print("Bad Frames =  " + format(badFrames, ',d'))
        missedFrames = (V[16] << 24)+(V[17] << 16)+(V[18] << 8)+V[19]
        print("Missed Frames =  " + format(missedFrames, ',d'))
        rfRelocks = (V[20] << 24)+(V[21] << 16)+(V[22] << 8)+V[23]
        print("RF re-locks =  " + format(rfRelocks, ',d'))


# start listening on multicast socket
print("initializing socket to listen to 225.1.1.1:7421")
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
if IS_ALL_GROUPS:
    # on this port, receives ALL multicast groups
    sock.bind(('', MCAST_PORT))
else:
    # on this port, listen ONLY to MCAST_GRP
    sock.bind((MCAST_GRP, MCAST_PORT))
mreq = struct.pack("4sl", socket.inet_aton(MCAST_GRP), socket.INADDR_ANY)

sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

msgCounter = 0
while True:  # here we start listening on the socket
    x = sock.recv(10240)
    msgCounter = msgCounter+1
    t = 1
    p = 0
    StartWithCAFE = False
    E8 = True
    print("----   "+str(datetime.now()) +
          "  msgCounter = "+str(msgCounter)+"   ----")
    while t != 0:  # parsing the TLV until message with Type =0
        # t=int(ord(x[p]))
        t = x[p]
        if t == 0:
            break  # stop on type ==0
        if p == 0 and t != 255:
            print("first TLV is not CAFE")
            exit(11)
        l = x[p+1]
        v = []  # array for the value
        for pp in range(0, l):  # from beginning of message +2 for l bytes
            v.append(x[p+2+pp])
        # per type handling
        if t != 232:
            SDDParser(t, l, v)
        if t == 232 and E8:  # see above about E8
            SDDParser(t, l, v)
            E8 = False
        p = p+l+2
    print()
