"""
MATYPE-1 (1 bytes)
MATYPE-1 field mapping
TS/GS 						SIS/MIS 			CCM/ACM 	ISSYI 			NPD 			RO
11 = Transport				1 = single			1 = CCM		1 = active		1 = active		00 = 0,35
00 = Generic Packetized		0 = multiple		0 = ACM		0 = not-active	0 = not-active	01 = 0,25
01 = Generic continuous																		10 = 0,20
10 = reserved																				11 = reserved 

 MATYPE-2 (1 bytes) - Multiple Input Stream, then second byte = Input Stream Identifier (ISI); else second byte
reserved. 

UPL (2 bytes): User Packet Length in bits, in the range 0 to 65 535. - 0x0000 = continuous stream. 
DFL (2 bytes): Data Field Length in bits, in the range 0 to 58 112. - BBFLength
SYNC (1 byte): copy of the User Packet Sync-byte. Continuous Generic Streams: SYNC= 00.  In Ayecka - counter
SYNCD (2 bytes): Continuous Generic Streams: SYNCD= 0000
CRC-8 (1 byte): error detection code applied to the first 9 bytes of the BBHEADER
 

--L2x         28(0x1C)        |  Sync = 0xC9  |    PLSCode    | Annex-M Slice |    RESERVED   |
--Data                        +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--            32              |PLFrame Counter|  MAType0      | MAType1       | UPL0          |
--                            +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--            36              |UPL1           |  DFL0         | DFL1          | Sync          |
--                            +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--                            |Syncd0         |  Syncd1       | CRC8          | tp cntr 3     |
--                            +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--                            |tp cntr 2      |  tp cntr 1    |tp cntr 0      |DFLength1      |
--                            +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--                            |DFLength0      |  00           | 01            | 02            |
--                            +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

UDP packet len = BBFLen (self.payloadSize) (includes BBFH and Test patter) + 5 bytes of L.2x
TestPattern
32 bit counter
16 bit BBFrame Length - includign header
total header 4+2 byte
000102030
"""
import struct
from Pdu import Pdu


class BaseFrame(Pdu):
    frameNumber = 0
    packetsPLSLen = {7: 384, 5: 2001, 31: 1464, 41: 7184, 93: 7274}

    def __init__(self, pls: int, slice: int = 1) -> None:
        super().__init__()
        if not pls in BaseFrame.packetsPLSLen.keys():
            raise Exception("PLS not supported")
        self.pls = pls
        self.slice = slice
        self.payloadSize = BaseFrame.packetsPLSLen[pls]

    def Build(self) -> bytearray:
        # https://www.etsi.org/deliver/etsi_en/302300_302399/302307/01.02.01_60/en_302307v010201p.pdf
        # matype = 0x6000
        self.pdu += struct.pack('>H', 0x6000)  # Generic continuous, RO 35 isi0

        # UPL = 0x0000
        self.pdu += struct.pack('>h', (0x0000))

        # DFL
        self.pdu += struct.pack('>H', (self.payloadSize-10)*8)

        # Frame counter
        self.pdu += struct.pack('>B', BaseFrame.frameNumber % 256)
        BaseFrame.frameNumber += 1

        # Syncd
        self.pdu += struct.pack('>h', 0x0000)

        # crc -  Checksum - see here https://crccalc.com/  -  error detection code applied to the first 9 bytes of the BBHEADER
        self.pdu += struct.pack('>B', self.crc8_dvb_s2(self.pdu))
        return self.pdu

    def crc8_dvb_s2(self, data: bytearray) -> int:
        crc = 0
        for a in data:
            crc ^= a  # Bitwise Exclusive Or
            for _ in range(8):
                if crc & 0x80:
                    crc = ((crc << 1) ^ 0xD5) % 256
                else:
                    crc = (crc << 1) % 256
        return crc

    @property
    def PayloadSize(self) -> int:
        return self.payloadSize

    @staticmethod
    def PayloadSize(pls: int) -> int:
        if not pls in BaseFrame.packetsPLSLen.keys():
            raise Exception('PLS not supported')

        return (BaseFrame.packetsPLSLen[pls])

    @staticmethod
    def HeaderSize() -> int:
        return 10


def main():
    bf = BaseFrame(5)
    bbmsg = bf.Create(bytearray(1, 2, 3, 4, 5))
    pass


if __name__ == '__main__':
    main()
