class Tlv:
    @staticmethod
    def create(t, v) -> object:
        tlv = Tlv()
        tlv.t = t
        tlv.l = len(v)
        tlv.v = v
        return tlv

    def __init__(self):
        self.t: int = 0
        self.l: int = 0
        self.v: bytearray = bytearray()
        self.buffer: bytearray = bytearray()
        self.offset = 0

    def append(self, data: int):
        self.v.append()(data)

    def serialize(self, t: int, v: bytearray = None) -> bytearray:
        o = bytearray()
        self.t = t
        if v is not None:
            self.v = v
        o.append(t)
        o.append(len(self.v))
        o += self.v
        return o

    def load(self, buffer: bytearray, offset: int = 0):
        self.buffer = buffer
        self.offset = offset

    def get_next(self) -> bool:
        if self.offset >= len(self.buffer):
            return False

        self.t = self.buffer[0+self.offset]
        self.l = self.buffer[1+self.offset]
        self.v = self.buffer[2+self.offset:2+self.offset+self.l]
        self.offset += self.l + 2
        return True

    def __str__(self) -> str:
        return f'Type  : {self.t}\nLength: {self.l}\nValue : {self.v}'


def main():
    l = [0xff, 2, 0xca, 0xfe, 1, 4, 1, 2, 3, 4,
         0xe8, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    buffer = bytearray(l)
    t1 = Tlv.getInstace(1, buffer)
    tlv = Tlv()

    b = tlv.serialize(7, bytearray([1, 2, 3, 4, 5, 6, 7, 8]))
    print(tlv)
    while tlv.get_next(buffer):
        print(tlv.t, tlv.l, tlv.v)


if __name__ == '__main__':
    main()
