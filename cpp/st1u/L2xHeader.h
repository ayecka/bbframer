#pragma once
#include "Pdu.h"

class L2xHeader :
	public Pdu
{
public:
	L2xHeader(int Pls, int Slice = 1);
	~L2xHeader();
	Pdu &Build();
	static int HeaderSize() { return 5; }

private:
	int pls;
	int slice;

private:
	static int frameCount;
};

