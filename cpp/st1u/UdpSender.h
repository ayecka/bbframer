#pragma once
#include<stdio.h>
#include "SpecialQueue.h"
#include <vector>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#include<winsock2.h>
#include <Ws2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")
#else
#define closesocket close
#include<arpa/inet.h>
#include<sys/socket.h>
#endif
#define MAXLEN 1500
class UdpSender
{
public:
	UdpSender(const char *host, int port, int src_port = 0, int QueueSize = 1000000);
	~UdpSender();
	void Add(std::vector<unsigned char> &data);
	int Send(std::vector<unsigned char> data);
	int Send(const char* data, int len);
	void Close();
	void SendWorker();

private:
	SpecialQueue<std::vector<unsigned char>> tx_queue;
	bool stopRun = false;
	std::thread *tx_thread = nullptr;

private:
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
	WSADATA wsa;
#endif
	int sockfd;
	char buffer[MAXLEN];
	SOCKET sock = 0;
	sockaddr_in dest;
	sockaddr_in local;
	
};

