#pragma once
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

template <typename T>
class SpecialQueue
{
public:

	T pop()
	{
		std::unique_lock<std::mutex> mlock(mutex_);
		if (queue_.empty())
		{
			pop_cond_.wait(mlock);
		}
		if (queue_.empty())
		{
			push_cond_.notify_one();
			return nullptr;
		}
		auto item = queue_.front();
		queue_.pop();
		push_cond_.notify_one();
		return item;
	}

	void pop(T& item)
	{
		std::unique_lock<std::mutex> mlock(mutex_);
		if (queue_.empty())
		{
			pop_cond_.wait(mlock);
		}
		if (queue_.empty())
		{
			item = (T)0;
		}
		else
		{
			item = queue_.front();
			queue_.pop();
		}
		push_cond_.notify_one();
	}

	void push(const T& item)
	{
		std::unique_lock<std::mutex> mlock(mutex_);
		if (queue_.size() > size)
		{
			//std::this_thread::sleep_for(std::chrono::microseconds(100));
			push_cond_.wait(mlock);
		}
		queue_.push(item);
		//mlock.unlock();
		pop_cond_.notify_one();
		
	}

	void push(T&& item)
	{
		std::unique_lock<std::mutex> mlock(mutex_);
		if (queue_.size() > size)
		{
			push_cond_.wait(mlock);
		}
		queue_.push(std::move(item));
		//mlock.unlock();
		pop_cond_.notify_one();
	}

	void SetQueueSize(int Size) { size = Size; }

	void Trigger() 
	{ 
		//std::unique_lock<std::mutex> mlock(mutex_); 
		pop_cond_.notify_all();
		push_cond_.notify_all();
	}

private:
	std::queue<T> queue_;
	std::mutex mutex_;
	std::condition_variable pop_cond_;
	std::condition_variable push_cond_;
	int size = 100;
};