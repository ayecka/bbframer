#pragma once
#include <vector>



class Pdu
{
public:
	Pdu(int offset = 0);
	~Pdu();
	virtual int size() { return (int)pdu.size(); }
	std::vector<unsigned char> &data(){ return pdu; }
	Pdu &Add(std::vector<unsigned char> &next_pdu, int size = 0);
	Pdu &Add(const unsigned char c);
	Pdu &Add(const unsigned short s);
	Pdu &Add(const unsigned int i);
	Pdu& operator+(std::vector<unsigned char> &next_pdu);
	Pdu& operator+(Pdu& next);
	const char &get_pData();
	//virtual Pdu& Build();

protected:
	std::vector<unsigned char> pdu;
};

