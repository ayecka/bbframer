//#include "stdafx.h"
#include "TestPattern.h"
#include "L2xHeader.h"
#include "BaseFrame.h"

#define MAX_TEST_BUFFER_SIZE 10000

using namespace std;

int TestPattern::frameCount = 0;
vector<unsigned char> TestPattern::testData;



void TestPattern::BuildPatter()
{
	for (int i = 0; i < MAX_TEST_BUFFER_SIZE; i++)
	{
		testData.push_back((unsigned char)(i % 256));
	}
}

TestPattern::TestPattern(int Pls, unsigned short PayloadSize, unsigned char BBHLen, int Slice)
{
	
	payloadSize = PayloadSize;
	bbHLen = BBHLen;
	pls = Pls;
	slice = Slice;
	if (PayloadSize == 0)
		payloadSize = (unsigned short)BaseFrame::GetPayloadSize(pls);

	pdu.reserve(payloadSize+L2xHeader::HeaderSize());

	if (frameCount == 0)
	{
		TestPattern::BuildPatter();
	}
}


TestPattern::~TestPattern()
{
}

Pdu &TestPattern::Build()
{
	L2xHeader l(pls, slice);
	BaseFrame b(pls, slice);
	
	Add(l.Build().data());
	Add(b.Build().data());

	//Counter
	frameCount = 0x12345678;
	Add((unsigned int)(frameCount % 0xFFFFFFFF));
	

	//Length excludign the BBFHeader includign the test pattern header
	Add(payloadSize);

	//Data

	Add(testData, payloadSize - bbHLen - 6);
	
	return *this;
}