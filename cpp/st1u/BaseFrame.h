#pragma once
#include "Pdu.h"
#include <list>
#include <map>

#define BASE_FRAME_HEADER_SIZE	10

class BaseFrame :
	public Pdu
{
public:
	BaseFrame(int Pls, int Slice=1);
	~BaseFrame();
	Pdu &Build();
	int GetPayloadSize() { return payloadSize; }
	static int GetPayloadSize(int Pls); 
	static bool PlsExists(int pls);
	static int HeaderSize() { return 10; }

private:
	unsigned char Crc8DvbS2(std::vector<unsigned char> &data);

private:
	static int frameCount;
	int payloadSize;

public:
	static std::map<int, int> packetsPLSLen;
};

