//#include "stdafx.h"
#include "UdpSender.h"

using namespace std;

UdpSender::UdpSender(const char *host, int port, int src_port, int QueueSize)
{
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
	WSAStartup(MAKEWORD(2, 2), &wsa);
#endif
	local.sin_family = AF_INET;
	inet_pton(AF_INET, 0, &local.sin_addr.s_addr);
	local.sin_port = htons(src_port);
	
	dest.sin_family = AF_INET;
	inet_pton(AF_INET, host, &dest.sin_addr.s_addr);
	dest.sin_port = htons(port);
	
	sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	
	//set SO_SNDBUF to work with pause frames
	int optval = 512;
	setsockopt(sock, SOL_SOCKET, SO_SNDBUF,(char *)&optval, sizeof(int));

	//bind
	bind(sock, (sockaddr *)&local, sizeof(local));

	tx_queue.SetQueueSize(QueueSize);
	tx_thread = new std::thread(&UdpSender::SendWorker, this);
}

void UdpSender::Add(vector<unsigned char> &data)
{
	tx_queue.push(data);
}

int UdpSender::Send(vector<unsigned char> data)
{
	vector <unsigned char>::iterator it = data.begin();
	return Send((const char *)&(*it), (int)data.size());
}

int UdpSender::Send(const char* data, int len)
{
	return sendto(sock, data, len, 0, (sockaddr *)&dest, sizeof(dest));
}

void UdpSender::Close()
{
	stopRun = true;
	tx_queue.Trigger();
	//tx_thread->join();
	
	closesocket(sock);
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
	WSACleanup();
#endif
	sock = 0;
}

UdpSender::~UdpSender()
{
	if (sock != 0)
		Close();
}

void UdpSender::SendWorker()
{
	stopRun = false;
	while (!stopRun)
	{
		vector<unsigned char> v;
		tx_queue.pop(v);
		if (v != (vector<unsigned char>)0)
			Send(v);
	}
}