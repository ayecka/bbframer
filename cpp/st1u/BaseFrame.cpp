//#include "stdafx.h"
#include "BaseFrame.h"

using namespace std;

int BaseFrame::frameCount = 0;
std::map<int, int> BaseFrame::packetsPLSLen = {
	{ 7,  384 },
	{ 5,  2001},
	{ 31, 1464},
	{ 41, 7184},
	{ 93, 7274}
};


BaseFrame::BaseFrame(int Pls, int Slice)
{
	if (PlsExists(Pls))
	{
		payloadSize = packetsPLSLen[Pls];
	}
}


BaseFrame::~BaseFrame()
{
}


bool BaseFrame::PlsExists(int pls)
{
	if (packetsPLSLen.find(pls) == packetsPLSLen.end()) 
	{
		return false;
	}
	return true;
}

Pdu& BaseFrame::Build()
{
	// https://www.etsi.org/deliver/etsi_en/302300_302399/302307/01.02.01_60/en_302307v010201p.pdf
	// matype = 0x6000
	Add((const unsigned short)0x6000);

	// UPL = 0x0000
	Add((const unsigned short)0x0000);

	// DFL
	Add((const unsigned short)((payloadSize - BASE_FRAME_HEADER_SIZE) * 8));

	//Frame counter
	Add((const unsigned char)(frameCount % 256));
	frameCount++;

	//Syncd
	Add((const unsigned short)(0x0000));

	// crc - Checksum - see here https ://crccalc.com/  -  error detection code applied to the first 9 bytes of the BBHEADER
	Add((const unsigned char)Crc8DvbS2(pdu));
	
	return *this;
}

unsigned char BaseFrame::Crc8DvbS2(vector<unsigned char> &data)
{
	unsigned char crc = 0;
	for (auto c : data)
	{
		crc ^= c;  // Bitwise Exclusive Or
		for (int i = 0; i < 8; i++) //_ in range(8) :
		{
			if (crc & 0x80)
				crc = ((crc << 1) ^ 0xD5) % 256;
			else
				crc = (crc << 1) % 256;
		}
	}
	return crc;
}

int BaseFrame::GetPayloadSize(int Pls)
{ 
	if (!BaseFrame::PlsExists(Pls))
		return 0;
	return BaseFrame::packetsPLSLen[Pls];
}