//#include "stdafx.h"
#include "L2xHeader.h"

using namespace std;

int L2xHeader::frameCount = 0;

L2xHeader::L2xHeader(int Pls, int Slice)
{
	pls = Pls;
	slice = Slice;
}


L2xHeader::~L2xHeader()
{
}

Pdu &L2xHeader::Build()
{
	// L.3 header = L.2
	Add((const unsigned char)0xC9);
	Add((const unsigned char)pls);
	Add((const unsigned char)slice);
	Add((const unsigned char)0); //reserved
	
	Add((const unsigned char)(frameCount % 0xff));
	frameCount++;

	return *this;
}