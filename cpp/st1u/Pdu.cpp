//#include "stdafx.h"
#include "Pdu.h"


using namespace std;

Pdu::Pdu(int offset)
{
	for (int i = 0; i < offset; i++)
	{
		pdu.push_back(0);
	}
}

Pdu::~Pdu()
{
}

Pdu &Pdu::Add(std::vector<unsigned char> &next_pdu, int size)
{ 
	if (size == 0)
	{
		size = next_pdu.size();
	}

	vector<unsigned char>::iterator it;
	it = next_pdu.begin();
	for (int i = 0; i < size; i++)// & it != next_pdu.end())
	{
		pdu.push_back(*it++);
	}
	return *this;
}

Pdu &Pdu::Add(const unsigned char c)
{
	pdu.push_back(c);
	return *this;
}

Pdu &Pdu::Add(const unsigned short s)
{
	pdu.push_back(unsigned char((s >> 8) & 0xFF));
	pdu.push_back(unsigned char(s & 0xFF));
	
	return *this;
}

Pdu &Pdu::Add(const unsigned int i)
{
	const unsigned char lolo = (i >> 0) & 0xFF;
	const unsigned char lohi = (i >> 8) & 0xFF;
	const unsigned char hilo = (i >> 16) & 0xFF;
	const unsigned char hihi = (i >> 24) & 0xFF;

	Add(hihi);
	Add(hilo);
	Add(lohi);
	Add(lolo);

	return *this;
}

Pdu& Pdu::operator+(std::vector<unsigned char> &next_pdu)
{

	Add(next_pdu, next_pdu.size());
	return *this;
}

Pdu& Pdu::operator+(Pdu& next)
{
	Add(next.pdu, next.size());
	return *this;
}

const char &Pdu::get_pData()
{
	vector <unsigned char>::iterator it = pdu.begin();
	return (const char )&(*it);
}