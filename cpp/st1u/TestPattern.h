#pragma once
#include "Pdu.h"
#include <vector>


class TestPattern :
	public Pdu
{
public:
	TestPattern(int Pls, unsigned short PayloadSize = 0, unsigned char BBHLen = 10, int Slice = 1);
	~TestPattern();
	int size() override { return (int)payloadSize; }
	Pdu &Build();


private:
	static void BuildPatter();

private:
	unsigned short payloadSize;
	unsigned char bbHLen;
	int pls;
	int slice;


private:
	static int frameCount;
	static std::vector<unsigned char> testData;
};

