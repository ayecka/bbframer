// tester.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//#include "pch.h"
#include <iostream>
#include "../st1u/TestPattern.h"
#include "../st1u/BaseFrame.h"
#include "../st1u/L2xHeader.h"
#include "../st1u/Pdu.h"
#include "../st1u/UdpSender.h"

#define VERSION "1.0"
using namespace std;

class InputParser {
public:
	InputParser(int &argc, char **argv) {
		for (int i = 1; i < argc; ++i)
			this->tokens.push_back(std::string(argv[i]));
	}
	/// @author iain
	const std::string& getCmdOption(const std::string &option) const {
		std::vector<std::string>::const_iterator itr;
		itr = std::find(this->tokens.begin(), this->tokens.end(), option);
		if (itr != this->tokens.end() && ++itr != this->tokens.end()) {
			return *itr;
		}
		static const std::string empty_string("");
		return empty_string;
	}
	/// @author iain
	bool cmdOptionExists(const std::string &option) const {
		return std::find(this->tokens.begin(), this->tokens.end(), option)
			!= this->tokens.end();
	}
private:
	std::vector <std::string> tokens;
};

void printHelp()
{
	cout << "tester version " << VERSION << endl;
	cout << "-h\t\tHelp" << endl;
	cout << "-H <modem ip address>\t\tThe modem ip address to receive the traffic []" << endl;
	cout << "-p <modem ip port>\t\tThe modem ip address to receive the traffic [1234]" << endl;
	cout << "-s <source port>\t\tThe test source poort [5432]" << endl;
	cout << "-n <number>\t\tThe number of packets to send [1000]" << endl;
	cout << "-q <number>\t\tThe transmit queue size [1000000]" << endl;
	cout << "-v \t\tVersobe. Show rotation cursor [n]" << endl;
	cout << endl;
	cout << "\ttester.exe -H 192.168.4.1 -n 100000 -v -q 10" << endl;
}

void DisplayRotation()
{
	static char cur[8] = { '-', '\\', '|', '/', '-','\\', '|','/' };
	static int index = 0;

	cout << "\r" << cur[index];
	index++;
	if (index >= 8) index = 0;
}

#define PBSTR "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBWIDTH 60

void printProgress(double percentage) 
{
	int val = (int)(percentage * 100);
	int lpad = (int)(percentage * PBWIDTH);
	int rpad = PBWIDTH - lpad;
	printf("\r%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
	fflush(stdout);
}

int main(int argc, char * argv[])
{
	int port = 1234;
	int src_port = 5432;
	int frames = 1000;
	int qSize = 1000000;
	bool verbose = false;

	InputParser input(argc, argv);
	if (input.cmdOptionExists("-h") | (!input.cmdOptionExists("-H")))
	{
		printHelp();
		exit(0);
	}

	const char *host = input.getCmdOption("-H").c_str();
	if (input.cmdOptionExists("-p"))
	{
		port = atoi(input.getCmdOption("-p").c_str());
	}

	if (input.cmdOptionExists("-s"))
	{
		src_port = atoi(input.getCmdOption("-s").c_str());
	}

	if (input.cmdOptionExists("-n"))
	{
		frames = atoi(input.getCmdOption("-n").c_str());
	}
	
	if (input.cmdOptionExists("-q"))
	{
		qSize = atoi(input.getCmdOption("-q").c_str());
	}

	

	if (input.cmdOptionExists("-v"))
	{
		verbose = true;
	}

	UdpSender udp(host, port, src_port, qSize);
	int count;
	for (count = 0; count < frames; count++)
	{
		auto p = TestPattern(5).Build();
		//udp.Send(p.get_pData(), p.size());
		udp.Add(p.data());

		if (verbose)
		{
			//DisplayRotation();
			if (count % 1000 == 0)
			{
				printProgress(float(count) / frames);
			}
		}
	}
	printProgress(1);
	udp.Close();
    std::cout << endl << "End of test " << count  << " packets sent" << endl; 
}
