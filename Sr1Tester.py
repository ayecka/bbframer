from logging import getLogger
from threading import Thread, Event
from datetime import datetime
from Pdu import Pdu
from L2xHeader import L2xHeader
from BaseFrame import BaseFrame
from UdpSender import UdpSender
from TestPattern import TestPatern
from time import sleep
from logging import getLogger, Logger
from traceback import print_exc
from sys import stdout


class Sr1Tester(Thread):

    def __init__(self, pls: int, host: str = '192.168.2.102', gap: float = 0.001, dport: int = 1234, sport: int = 5432):
        super(Sr1Tester, self).__init__()
        self.daemon = True
        self.stopRun: Event = Event()
        self.log: Logger = getLogger(__name__)
        self.pls: int = pls
        self.host: str = host
        self.gap: float = gap
        self.sport: int = sport
        self.dport: int = dport
        
        self.results: str = ""
        self.saqmples: int = 0
        self.packet_sent: int = 0
        self.total_time: float = 0.0
        self.bps: int = 0
        
        self.sender: UdpSender = UdpSender("0.0.0.0", dport, 100)
       
        

    def Start(self, samples: int) -> bool:
        if self.is_alive():
            return False
        self.samples = samples
        self.start()
        self.sender.Start()
        return True

    def Stop(self) -> bool:
        self.stopRun.set()
        if self.is_alive():
            self.join(1)
        return self.is_alive()

    def BuildTestPdu(self) -> Pdu:
        p = Pdu()
        l2 = L2xHeader(self.pls)
        p += l2.Build()
        bb = BaseFrame(self.pls)
        p += bb.Build()
        p += TestPatern(BaseFrame.PayloadSize(self.pls)-bb.size).Build()
        return p

    def BuildTestBytesarray(self) -> bytearray:
        return L2xHeader(self.pls).Build() + BaseFrame(self.pls).Build() + TestPatern(BaseFrame.PayloadSize(self.pls), 10).Build()

    def get_result(self):
        return self.results

    def finish_to_send(self):
        return self.sender.Empty()

    def get_packet_sent(self) -> int:
        return self.packet_sent

    def get_total_time(self) -> float:
        return self.total_time

    def get_bps(self) -> int:
        return self.bps
    
    def run(self):
        self.stopRun.clear()
        try:
            start = datetime.now()
            for x in range(self.samples):
                if self.stopRun.is_set():
                    self.results = "Interruped during test\n"
                    self.log.info(f'Interruped during test.')
                    return

                p = self.BuildTestBytesarray()
                self.sender.Add(self.host, self.dport, p)
                self.packet_sent += 1
                if self.gap > 0:
                    sleep(self.gap)

            end = datetime.now()
            self.total_time = (end-start).total_seconds()
            self.results = (f'time : {self.total_time}\n')
            self.results += (f'pps: {int((x+1)/self.total_time):,}\n')
            packer_size_in_bits = (len(p) + 14 + 20 + 8) * 8  # (MAC + IP + UDP) in bits
            self.bps = int(((x+1)*packer_size_in_bits)/self.total_time)
            self.results += (f'bps: {int(((x+1)*packer_size_in_bits)/self.total_time):,}\n')

            self.log.info(f'time : {self.total_time}')
            self.log.info(f'pps: {int((x+1)/self.total_time):,}')
            self.log.info(
                f'bps: {int(((x+1)*packer_size_in_bits)/self.total_time):,}')
        except Exception as ex:
            print(ex)
            print_exc(limit=10, file=stdout)
            self.log.error('Failed to execute test', stack_info=True)
