import socket
import struct


class UdpMulticastReceiver:
    def __init__(self, group: str, port: int = 7421,  recv_all: bool = True) -> None:
        self.group = group
        self.port = port
        self.recv_all = recv_all
        self.sock = self.create_socker()

    def create_socker(self) -> socket:
        sock: socket = socket.socket(
            socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # on this port, receives ALL multicast groups if group = ''
        if self.recv_all:
            sock.bind(('', self.port))
        else:
            sock.bind((self.group, self.port))

        mreq = struct.pack("4sl", socket.inet_aton(
            self.group), socket.INADDR_ANY)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        return sock

    def Recv(self, length: int = 10240) -> bytes:
        return self.sock.recv(length)

    def Close(self):
        if self.sock is not None:
            self.sock.close()
            self.sock = None
