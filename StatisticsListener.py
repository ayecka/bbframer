from UdpMulticastReceiver import UdpMulticastReceiver
from Sdd import Sdd
from Tlv import Tlv
from datetime import datetime
from threading import Thread, Event


class StatisticsListener(Thread):
    MCAST_GRP = '225.1.1.1'  # hard coded in SR1Pro
    MCAST_PORT = 7421

    def __init__(self, print_to_console: bool = True, group: str = MCAST_GRP, port=MCAST_PORT) -> None:
        super(StatisticsListener, self).__init__()
        self.sock = UdpMulticastReceiver(group, port)
        self.sdd = Sdd()
        self.stopThread: Event = Event()
        self.msgCounter = 0
        self.print_to_console = print_to_console
        self.daemon = True
        self.result: str = ""

    def Stop(self):
        self.stopThread.set()
        if self.is_alive:
            self.join(1)

    def get_result(self) -> str:
        return self.result

    def get_frame_count(self) -> int:
        return self.msgCounter

    def run(self):
        self.stopThread.clear()
        self.msgCounter = 0
        while not self.stopThread.is_set():
            x = self.sock.Recv(10240)
            if x is None:
                continue
            self.msgCounter += 1

            if self.print_to_console:
                print(chr(27) + "[2J")  # cls
                print(
                    f'----   {datetime.now()}  msgCounter = {self.msgCounter}   ----')
            tlv = Tlv()
            tlv.load(x)
            tlv.get_next()
            self.sdd.Parser(tlv.t, tlv.l, tlv.v)
            if not self.sdd.cafe:
                print("first TLV is not CAFE")
                continue
            while tlv.get_next():  # tlv.t != 0:
                ret_str = self.sdd.Parser(tlv.t, tlv.l, tlv.v)
                # if ret_str is not None:
                #    print (ret_str)

            # self.result = (chr(27) + "[2J") # cls
            self.result = (
                f'----   {datetime.now()}  message counter = {self.msgCounter}   ----\n\n')

            self.result += (f'FPGA Version   : {self.sdd.FpgaVersion}\n')
            self.result += (f'EsNo           : {self.sdd.Esno}\n')
            self.result += (f'RF Relocks     : {self.sdd.rfRelocks}\n')
            self.result += (f'---------------------------------------------------\n')
            self.result += (
                f'\tBiterate     : {self.sdd.testPatternBitRate:,}\n')
            self.result += (f'\tGood frames  : {self.sdd.goodFrames:,}\n')
            self.result += (f'\tMissed frames: {self.sdd.missedFrames:,}\n')
            self.result += (f'\tBad frames   : {self.sdd.badFrames:,}\n')
